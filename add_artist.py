#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
import optparse
import os
import sys
from func import is_mp3
try:
    from mutagen.id3 import ID3, ID3NoHeaderError, TPE1, TPE2
except ImportError:
    sys.exit("Ошибка импорта. Установите Mutagen.")

__author__ = "samssrus (samssrus@gmail.com)"
__version__ = "0.1.1"

"""
Скрипт для добавления (изменения) id3-тега АРТИСТ в mp3-файлах.
Использование:
python3 add_artist.py -f path/to/file.mp3 -a artist_name

Распространяется по лицензии MIT. Подробности смотрите в файле LICENSE.
"""


def add_artist(file, artist):
    """
    функция непосредственно изменяющая id3-тег
    """
    try:
        tags = ID3(file)
    except ID3NoHeaderError:
        tags = ID3()
    tags["TPE1"] = TPE1(encoding=3, text=artist)  # тег артист
    tags["TPE2"] = TPE2(encoding=3, text=artist)  # тег артист
    tags.save(file)
    print(u"Информация об Артисте (%s) успешно добавлена в файл %s" %
          (artist, file))


def check_data(file, artist):
    """проверка достаточности входных данных"""
    if file is None:
        sys.exit("Не указан файл для обработки. Ключ -f")
    if not os.path.exists(file):
        sys.exit("Файл указанный для обработки не существует. Проверь путь.")
    if not is_mp3(file):
        sys.exit("Файл указанный для обработки не MP3. Выберите другой файл.")
    if artist is None:
        sys.exit("Не указано название исполнителя. Ключ -a")


def main():
    p = optparse.OptionParser()
    p.add_option("-f", "--file", action="store", type="string",
                 dest="infile")  # файл для обработки
    p.add_option("-a", "--artist", action="store", type="string",
                 dest="inartist")  # название артиста
    opt, args = p.parse_args()

    check_data(opt.infile, opt.inartist)
    add_artist(opt.infile, opt.inartist)

if __name__ == "__main__":
    main()
