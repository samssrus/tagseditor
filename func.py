#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
import os

__author__ = "samssrus (samssrus@gmail.com)"
__version__ = "0.1.1"

"""
Вспомогательные функции для скриптов изменения id3-тегов в mp3-файлах.

Распространяется по лицензии MIT. Подробности смотрите в файле LICENSE.
"""

def is_ext(ext, filename):
    """
    функция проверки файла на допустимые типы по расширению

    параметры:
    ext      -- tuple кортеж с расширениями для проверки
    filename -- string путь к проверяемому файлу

    returns:
    возвращает значение True, если расширение файла filename соответствует хотябы одному из значений в переданном кортеже ext.
    """
    extension = os.path.splitext(filename)[1][1:]
    f_name = os.path.splitext(filename)[0]
    if extension.lower() in ext:
        return True
    else:
        return False


def is_mp3(filename):
    """
    функция проверки файла на тип mp3

    параметры:
    filename -- string путь к проверяемому файлу

    returns:
    возвращает значение True, если расширение файла filename mp3.
    """
    return is_ext(("mp3"), filename)


def is_image(filename):
    """
    функция проверки файла на тип картинки

    параметры:
    filename -- string путь к проверяемому файлу

    returns:
    возвращает значение True, если расширение файла filename соответствует png, jpg или jpeg.
    """
    return is_ext(("png", "jpg", "jpeg"), filename)
