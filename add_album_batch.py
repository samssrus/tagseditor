#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
import optparse
import os
import sys
from func import is_mp3
from add_album import add_album

__author__ = "samssrus (samssrus@gmail.com)"
__version__ = "0.1.1"

"""
Скрипт для ПАКЕТНОГО добавления (изменения) id3-тега АЛЬБОМ в mp3-файлах.
Использование:
python3 add_album_batch.py -f path/to/folder -a album_name

Распространяется по лицензии MIT. Подробности смотрите в файле LICENSE.
"""


def add_album_batch(folder, album):
    """
    функция осуществляющая обход указанной директории и вызывающая функцию
    изменения id3-тега для найденных файлов.

    параметры:
    folder  -- string путь к директории с файлами для обработки
    album   -- string значение присваиваемое тегу АЛЬБОМ
    """
    for root, dirs, files in os.walk(folder):
        for name in files:
            if is_mp3(name):
                infile = os.path.join(root, name)
                add_album(infile, album)


def check_data(folder, album):
    """
    функция проверки достаточности принятых данных

    параметры:
    folder  -- string путь к директории с файлами для обработки
    album -- string значение присваиваемое тегу АЛЬБОМ
    """
    if folder is None:
        sys.exit("Не указана директория с файлами для обработки. Ключ -f")
    if not os.path.exists(folder):
        sys.exit("Указанная директория не существует. Проверь путь.")
    if album is None:
        sys.exit("Не указано название альбома. Ключ -a")


def main():
    """
    функция точки входа

    - принимаем начальные данные;
    - проверяем принятые данные;
    - вносим изменения.
    """
    parse = optparse.OptionParser(
        version=__version__, description="Скрипт для добавления (изменения) id3-тега в mp3-файле.")
    # папка с файлами для обработки
    parse.add_option("-f", "--folder", action="store", type="string",
                     dest="infolder", help="путь к директории с mp3-файлами для обработки")
    # название альбома
    parse.add_option("-a", "--artist", action="store", type="string",
                     dest="inalbum", help="название альбома")

    opt, args = parse.parse_args()                  # принимаем начальные данные
    """
    if len(args) <= 1:
        parse.error("Недостаточно начальных данных для внесения изменений.")
    """
    check_data(opt.infolder, opt.inalbum)       # проверяем принятые данные
    add_album_batch(opt.infolder, opt.inalbum)  # вносим изменения

if __name__ == "__main__":
    main()
