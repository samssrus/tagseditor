#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
import optparse
import os
import sys
from func import is_mp3
try:
    from mutagen.id3 import ID3, ID3NoHeaderError, TALB
except ImportError:
    sys.exit("Ошибка импорта. Установите Mutagen.")

__author__ = "samssrus (samssrus@gmail.com)"
__version__ = "0.1.1"

"""
Скрипт для добавления (изменения) id3-тега АЛЬБОМ в mp3-файле.
Использование:
python add_album.py -f path/to/file.mp3 -a album_name

Распространяется по лицензии MIT. Подробности смотрите в файле LICENSE.
"""


def add_album(infile, album):
    """
    функция непосредственно изменяющая id3-тег

    параметры:
    infile  -- string путь к редактируемому файлу
    album -- string значение присваиваемое тегу АЛЬБОМ
    """
    try:
        tags = ID3(infile)
    except ID3NoHeaderError:
        tags = ID3()
    tags["TALB"] = TALB(encoding=3, text=album)  # тег альбом
    tags.save(infile)
    print("Информация об альбоме %s успешно добавлена в файл %s" % (album, infile))


def check_data(infile, album):
    """
    функция проверки достаточности принятых данных

    параметры:
    infile  -- string путь к редактируемому файлу
    album -- string значение присваиваемое тегу АЛЬБОМ
    """
    if infile is None:
        sys.exit("Не указан файл для обработки. Ключ -f")
    if not os.path.exists(infile):
        sys.exit("Файл указанный для обработки не существует. Проверь путь.")
    if not is_mp3(infile):
        sys.exit("Файл указанный для обработки не MP3. Выберите другой файл.")
    if album is None:
        sys.exit("Не указано название альбома. Ключ -a")


def main():
    """
    функция точки входа

    - принимаем начальные данные;
    - проверяем принятые данные;
    - вносим изменения.
    """
    parse = optparse.OptionParser(
        version=__version__, description="Скрипт для добавления (изменения) id3-тега в mp3-файле.")
    parse.add_option("-f", "--file", action="store", type="string",
                     dest="infile", help="путь к mp3-файлу для обработки")  # файл для обработки
    parse.add_option("-a", "--album", action="store", type="string",
                     dest="inalbum", help="название альбома")               # название альбома

    opt, args = parse.parse_args()              # принимаем начальные данные
    """
    if len(args) <= 1:
        parse.error("Недостаточно начальных данных для внесения изменений.")
    """
    check_data(opt.infile, opt.inalbum)     # проверяем принятые данные
    add_album(opt.infile, opt.inalbum)      # вносим изменения

if __name__ == "__main__":
    main()
